# Zephr Decision SDK #

### Overview ###

Providing access to the Zephr Decision Engine using Feature SDK rules.

### Getting the SDK ###

This SDK is available as an NPM module.
```
npm install @zephr/sdk
```

### Usage ###

The SDK needs your Zephr Site Domain to operate
```
const Zephr = require('@zephr/sdk').build('my-zephr-site-domain');

var inputs = getInputs(); // you need to implement this

var response = await Zephr.decision(inputs);
```

Response in this instance will return a Json object detailing the sdkFeatureSlug, outputType and outputValue.
To retrieve just the outputValue an optional boolean can be added to the decision function.

```
var response = await Zephr.decision(inputs, true); // will return only outputValue
```
