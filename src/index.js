const axios = require('axios')

class Zephr {
    constructor(domain) {
        this.url = cleanUrl(domain)
    }

    overrideBaseUrl(url) {
        this.url = url
        return this
    }

    async decision(inputs, raw = false) {
        if (typeof inputs !== 'object') {
            throw new Error('\'inputs\' must be an Object')
        }
        if (!inputs.sdkFeatureSlug) {
            throw new Error('\'sdkFeatureSlug\' required in \'inputs\' argument')
        }

        let configUrl = `${this.url}/zephr/decision-engine`
        if (raw) {
            configUrl += '?raw'
        }

        const config = {
            url: configUrl,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            data: inputs
        }

        return axios(config)
            .then(({data}) => data)
            .catch(error => {
                console.log(error)
                return error
            })
    }
}

function cleanUrl(domain) {
    let cleanUrl = domain.replace(/\/$/, '')
    return /^.*:\/\//.test(cleanUrl)
        ? cleanUrl
        : `https://${cleanUrl}`
}

function build(domain) {
    return new Zephr(domain)
}

module.exports = {
    build
}
